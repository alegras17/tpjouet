package data;

import java.util.LinkedList;
import java.util.List;
import entites.Catalogue;
import entites.Categorie;
import entites.Jouet;
import entites.TrancheAge;

public class Donnees {
    
   private static Catalogue          leCatalogue          =  new Catalogue(2018);
   
   private static List<Jouet>        tousLesJouets        =  new LinkedList();
   private static List<TrancheAge>   toutesLesTranchesAge =  new LinkedList();
   private static List<Categorie>    toutesLesCategories  =  new LinkedList();
   
   static{
  
        Categorie    categ1, categ2;     
        Jouet        j1,j11,j12,j13,j2,j21,j22; 
        TrancheAge   t1,t2;
        
        categ1=new Categorie("MI","Miniature");
        categ2=new Categorie("JS","Jeu de société");  
        
        t1= new TrancheAge(1,4,10);
        t2= new TrancheAge(2,7,77);
        
        toutesLesTranchesAge.add(t1);
        toutesLesTranchesAge.add(t2);
        
        toutesLesCategories.add(categ1);
        toutesLesCategories.add(categ2);
        
        j1=new Jouet();j1.setNumero(1);j1.setLibelle("Voiture A");
        j1.setLaCategorie(categ1);j1.setLaTrancheAge(t1);
        j11=new Jouet();j11.setNumero(11);j11.setLibelle("Voiture B");
        j11.setLaCategorie(categ1);j11.setLaTrancheAge(t1);
        j12= new Jouet();j12.setNumero(12);j12.setLibelle("Voiture C");
        j12.setLaCategorie(categ1);j12.setLaTrancheAge(t1);
        j13= new Jouet();j13.setNumero(13);j13.setLibelle("Voiture D");
        j13.setLaCategorie(categ1);j13.setLaTrancheAge(t1);
        j2= new Jouet();j2.setNumero(2);j2.setLibelle("Monopoly");
        j2.setLaCategorie(categ2);j2.setLaTrancheAge(t2);
        j21= new Jouet();j21.setNumero(21);j21.setLibelle("Risk");
        j21.setLaCategorie(categ2);j21.setLaTrancheAge(t2);
        j22= new Jouet();j22.setNumero(22);j22.setLibelle("Catane");
        j22.setLaCategorie(categ2);j22.setLaTrancheAge(t2);
        
        
        
        categ1.ajouterJouet(j1);categ1.ajouterJouet(j11);
        categ1.ajouterJouet(j12);categ1.ajouterJouet(j13);

        categ2.ajouterJouet(j2);categ2.ajouterJouet(j21);
        categ2.ajouterJouet(j22);
        
        tousLesJouets.add(j1);
        tousLesJouets.add(j11); 
        tousLesJouets.add(j12); 
        tousLesJouets.add(j13);
        tousLesJouets.add(j2);
        tousLesJouets.add(j21);
        tousLesJouets.add(j22);
        
        leCatalogue.getLesJouets().put(j1, 12L );
        leCatalogue.getLesJouets().put(j11, 5L );
        leCatalogue.getLesJouets().put(j12, 10L );
        leCatalogue.getLesJouets().put(j13, 8L );
        leCatalogue.getLesJouets().put(j2, 10L );
        leCatalogue.getLesJouets().put(j21, 4L );
        leCatalogue.getLesJouets().put(j22, 8L );
  }    

   public static Catalogue getLeCatalogue() {return leCatalogue;}
   
 
   public static Jouet getJouetNumero(int numero){
   
      Jouet rj=null;
      
      for ( Jouet j : tousLesJouets){  if(j.getNumero()==numero){rj=j;break; }}
      
      return rj; 
   }
   
}
