package entites;

public class TrancheAge {

    private int      code;
    private int      ageMin;
    private int      ageMax;

    public TrancheAge(int code, int ageMin, int ageMax) {
       
        this.code   = code;
        this.ageMin = ageMin;
        this.ageMax = ageMax;
    }
       
    //<editor-fold defaultstate="collapsed" desc="gets & sets">
    
    public int getAgeMax() {
        return ageMax;
    }
    public void setAgeMax(int ageMax) {
        this.ageMax = ageMax;
    }
    public int getAgeMin() {
        return ageMin;
    }
    public void setAgeMin(int ageMin) {
        this.ageMin = ageMin;
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    //</editor-fold>

}
