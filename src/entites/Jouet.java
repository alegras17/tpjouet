package entites;
public class Jouet {

    private int            numero;
    private String         libelle;

    private Categorie      laCategorie;
    private TrancheAge     laTrancheAge;

    public Jouet() {
    }

    public Jouet(int numero, String libelle, Categorie laCategorie, TrancheAge laTrancheAge) {
        this.numero = numero;
        this.libelle = libelle;
        this.laCategorie = laCategorie;
        this.laTrancheAge = laTrancheAge;
        this.laCategorie.ajouterJouet(this);
    }
            
    public String getInfos(){  
        return libelle+":"+laCategorie.getLibelle()+":"+laTrancheAge.getAgeMin()+":"+laTrancheAge.getAgeMax();
    }

    public boolean convient(int age){        
        return age>=laTrancheAge.getAgeMin() && age<=laTrancheAge.getAgeMax();
    }

    //<editor-fold defaultstate="collapsed" desc="gets  et sets">
    
    public Categorie getLaCategorie() {
        return laCategorie;
    }
    
    public void setLaCategorie(Categorie laCategorie) {
        this.laCategorie = laCategorie;
    }
    
    public String getLibelle() {
        return libelle;
    }
    
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    
    public int getNumero() {
        return numero;
    }
    
    public void setNumero(int numero) {
        this.numero = numero;
    }
    
    public TrancheAge getLaTrancheAge() {
        return laTrancheAge;
    }
    
    public void setLaTrancheAge(TrancheAge laTrancheAge) {
        this.laTrancheAge = laTrancheAge;
    }
    //</editor-fold>
}
