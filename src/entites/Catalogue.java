package entites;

import java.util.HashMap;
import java.util.Map;

public class Catalogue {
  
    private int annee;
  
    private Map<Jouet,Long> lesJouets=new HashMap<Jouet,Long>();

    public int quantiteDistribuee(){
        int qte=0;
        
        
        for(Jouet j : lesJouets.keySet()){
            qte+=lesJouets.get(j);
        }        
        
        return qte;
    }
    
    
    
    
    
    public Map<Categorie,Long> StatCateg(){    
        Map<Categorie,Long> stat = new HashMap<Categorie,Long>();
        
        return stat;
    }
    
    
    public Catalogue(int annee) { this.annee = annee;}

    public void ajouterJouet (Jouet jouet,Long quantite){
        lesJouets.put(jouet, quantite);
    }
    
    //<editor-fold defaultstate="collapsed" desc="gets et sets">
    
    public int getAnnee() {
        return annee;
    }
    public void setAnnee(int annee) {
        this.annee = annee;
    }
    public Map<Jouet, Long> getLesJouets() {
        return lesJouets;
    }
    public void setLesJouets(HashMap<Jouet, Long> lesJouets) {
        this.lesJouets = lesJouets;
    }
    //</editor-fold>
}
