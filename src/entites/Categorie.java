
package entites;

import java.util.LinkedList;
import java.util.List;

public class Categorie {

    private String      code;
    private String      libelle;
    
    private List<Jouet> lesJouets= new LinkedList<Jouet>();

    public Categorie(String code, String libelle) {
       
        this.code    = code;
        this.libelle = libelle;
    }

    public void ajouterJouet(Jouet jouet){ lesJouets.add(jouet); }

    //<editor-fold defaultstate="collapsed" desc="gets  & sets">
    
    public String getCode() {
        return code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getLibelle() {
        return libelle;
    }
    
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    //</editor-fold>
}
