package essais;

import data.Donnees;
import entites.Catalogue;
import entites.Categorie;
import entites.Jouet;

public class Main {

    public static void main(String[] args) {

       
        Catalogue catal=Donnees.getLeCatalogue();
        
            
        System.out.println();
        
      
        for(Jouet j : catal.getLesJouets().keySet()){
        
       
            System.out.printf("%-20s %-20s de %2d à %2d ans %3d \n",
                              j.getLibelle(),
                              j.getLaCategorie().getLibelle(),
                              j.getLaTrancheAge().getAgeMin(),
                              j.getLaTrancheAge().getAgeMax(),
                              catal.getLesJouets().get(j)
            );
        }
        
            
        System.out.println();
        
        
    }
}